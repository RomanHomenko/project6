//
//  ViewController.swift
//  Project2
//
//  Created by Роман Хоменко on 27.03.2022.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var firstFlagButton: UIButton!
    @IBOutlet var secondFlagButton: UIButton!
    @IBOutlet var thirdFlagButton: UIButton!
    
    var countries = [String]()
    var score = 0
    var correctAnswer = 0
    
    var questionCounter = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(showScoresTapped))
        
        countries += ["estonia", "france", "germany", "ireland", "italy", "monaco", "nigeria", "poland", "russia", "spain", "us", "uk"]
        
        firstFlagButton.layer.borderWidth = 1
        secondFlagButton.layer.borderWidth = 1
        thirdFlagButton.layer.borderWidth = 1
        
        firstFlagButton.layer.borderColor = UIColor.lightGray.cgColor
        secondFlagButton.layer.borderColor = UIColor.lightGray.cgColor
        thirdFlagButton.layer.borderColor = UIColor.lightGray.cgColor
        
        askQuestion()
    }
    
    func askQuestion(action: UIAlertAction! = nil) {
        countries.shuffle()
        correctAnswer = Int.random(in: 0...2)
        
        firstFlagButton.setImage(UIImage(named: countries[0]), for: .normal)
        secondFlagButton.setImage(UIImage(named: countries[1]), for: .normal)
        thirdFlagButton.setImage(UIImage(named: countries[2]), for: .normal)
        
        title = "\(countries[correctAnswer].uppercased()), score: \(score)"
    }
    
    // MARK: - IBActions
    
    @IBAction func buttonTapped(_ sender: UIButton) {
        var title: String
        
        if sender.tag == correctAnswer {
            title = "Correct"
            score += 1
        } else {
            title = "Wrong, this is \(countries[sender.tag])'s flag"
            score -= 1
        }
        
        questionCounter += 1
        
        if questionCounter == 5 {
            let alert = UIAlertController(title: title, message: "Your final score is \(score)", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Reset", style: .default))
            present(alert, animated: true)
            
            questionCounter = 0
            score = 0
        }
        
        askQuestion()
    }
    
    @objc func showScoresTapped() {
        let alert = UIAlertController(title: "Scores", message: "Your score is \(score)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Continue", style: .default))
        present(alert, animated: true)
    }
    
}

